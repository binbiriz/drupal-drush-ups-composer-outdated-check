# Checking Remote Sites' Update Status

## Important Notice

`drupal_drush_ups_live.sh` file has been deprecated. A new version of this file (`drupal_drush_ups_json_live.sh`) will be used for 'Drush Version'.

## General Caution

> Don't forget to backup your sites prior to updates.
>
> Preferably never update your sites directly on the server, neither dev, staging nor live.
>
> First apply the updates on a local copy of each site, test the updates and then do it on the server.
>
> Before making the update, enable maintenance mode.
>
> Ideally work with a SCM system like git, and use a git workflow to maintain your sites.
>
> You won't regret! :)

## Table of Contents

1. Drush Version
2. Composer Version

## 1. Drush Version

`drupal_drush_ups_json_live.sh` is a simple bash script accompanied by a drush alias file for checking remote Drupal sites' update status.

> This solution is only tested with sites Drupal 7 and 8 that are **not** using dependency management with composer.
>
> This solution is only for Drush 8.

### Requirements

1. Drush 8
2. Remote shell access to the server(s) via SSH key
3. `jq` must be installed on your local machine for json parsing (`sudo apt install jq`)

### Environment Info

This script is tested in an environment below:

```bash
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.2 LTS
Release:	18.04
Codename:	bionic
```

```bash
Drush Version   :  8.1.16
```

```bash
PHP 7.2.15-0ubuntu0.18.04.1 (cli) (built: Feb  8 2019 14:54:22) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.15-0ubuntu0.18.04.1, Copyright (c) 1999-2018, by Zend Technologies
    with Xdebug v2.6.0, Copyright (c) 2002-2018, by Derick Rethans
```

### Features

1. Checks both Drupal 7 and Drupal 8 sites
2. Checks both enabled and disabled modules and themes (*see Picture 1*)
3. Writes bash command files in a directory called `./files/commands` for easy updates for both enabled and disabled items. (*Note: Disabled items are only downloaded via `drush`.*) (*see Picture 2*)

**Picture 1:**
![Drush Result File Sample](images/drush_json_result_file.png "Drush Result File Sample")

**Picture 2:**
![Drush Command File Sample](images/drush_json_command_file.png "Drush Command File Sample")

### Usage

1. Clone the repo
2. Copy `example.livesite.aliases.drushrc.php` as `livesite.aliases.drushrc.php`
3. Configure your aliases
4. Copy `example.drupal_drush_ups_live_alias_names.txt` as `drupal_drush_ups_live_alias_names.txt`
5. Enter all your alias names in this file (`drupal_drush_ups_live_alias_names.txt`) as a line for each and do not forget to add Drupal version like below:
>```txt
>livesite.myfirstsitealias,D7
>livesite.mysecondsitealias,D8
>```
6. Run `bash drupal_drush_ups_json_live.sh`
>**Warning!** This script clears all the caches prior to check updates.
7. After script execution finishes, you will get a list of updates in a csv file under `./files/` folder and
8. Drush command files for each alias under `./files/commands/`

## 2. Composer Version

`drupal_composer_outdated_json_live.sh` is a bash that runs a composer script on the remote server and populates update information from your composer-based Drupal 8 sites.

### Requirements

1. `composer` must be installed on the remote server
2. Remote shell access to the server(s) via SSH key
3. `jq` must be installed on your local machine for json parsing (`sudo apt install jq`)

### Usage

1. Clone the repo
2. Copy `example.drupal_composer_outdated_json_live_path_names.txt` as `drupal_composer_outdated_json_live_path_names.txt`
3. Enter all your remote site information in this file (`drupal_composer_outdated_json_live_path_names.txt`) as a line for each. **IMPORTANT! The order is `sitename,remote user name, remote host name, remote Drupal 8 root folder full path (this path is the path where web folder exists)`. They must be comma seperated and there must be no whitespace before and after the comma.**
4. Run `bash drupal_composer_outdated_json_live.sh`
5. After script execution finishes, you will get a list of updates in a csv file under `./files/` folder.
