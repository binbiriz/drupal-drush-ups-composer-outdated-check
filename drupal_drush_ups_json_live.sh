#!/bin/bash
# must be run like this
# bash drupal_drush_ups_json_live.sh

# Completely remove files and tmp files if needed
# rm ./files/tmp/l*
# rm ./files/2*

cp livesite.aliases.drushrc.php $HOME/.drush/livesite.aliases.drushrc.php
drush cc drush

mydate=$(date +%Y%m%d_%H%M%S)

# Creating aliases array with values coming from the text file.
mapfile -t aliases < drupal_drush_ups_live_alias_names.txt

myFileName=./files/$mydate-livesites-updates-result.csv

touch $myFileName
echo "SiteName,Date,ModulName,ExistingVersion,NewVersion,RecommendedVersion,Status,InstallationStatus" > $myFileName

for item in ${aliases[*]}
do
  IFS=',' read -r -a mySiteInfo <<< "$item"

  if [[ ${item:0:1} == "#" ]]  # commented line if check
  then
    # Skip it if commented
    echo "This one is skipped: ""$item"
  else
    myAlias=${mySiteInfo[0]}
    myType=${mySiteInfo[1]}
    echo "$myAlias is being checked..."
    if [ $myType == "D7" ]
    then
      drush @$myAlias cc all
    else
      drush @$myAlias cr
    fi

    myAliasFileTmp="./files/tmp/$myAlias-$mydate-full.json"
    myAliasFileTmpCSV="./files/tmp/$myAlias-$mydate-full.csv"
    drush @$myAlias ups --format=json --pipe --check-disabled > $myAliasFileTmp
    sed -i '/Kullanılabilir güncelleme/d' $myAliasFileTmp
    sed -i '/güncelleme verisi kontrol ediliyor /d' $myAliasFileTmp
    sed -i '/Checking/d' $myAliasFileTmp
    sed -i '/No release history was found for the requested project/d' $myAliasFileTmp
    sed -i '/Node no longer exists/d' $myAliasFileTmp
    sed -i '/The following module is missing/d' $myAliasFileTmp
    sed -i '/the documentation page/d' $myAliasFileTmp
    sed -i '/documentation page/d' $myAliasFileTmp
    sed -i '/page</a>/d' $myAliasFileTmp
    sed -i '/Project.php/d' $myAliasFileTmp

    myCommand="jq --raw-output 'to_entries[] | [.value.name, .value.existing_version, .value.latest_version, .value.recommended, .value.status_msg] | @csv' $myAliasFileTmp > $myAliasFileTmpCSV"
    eval $myCommand

    myAliasFileTmpEnabled="./files/tmp/$myAlias-$mydate-enabled.json"
    myAliasFileTmpEnabledCSV="./files/tmp/$myAlias-$mydate-enabled.csv"
    drush @$myAlias ups --format=json --pipe > $myAliasFileTmpEnabled
    sed -i '/Kullanılabilir güncelleme/d' $myAliasFileTmpEnabled
    sed -i '/güncelleme verisi kontrol ediliyor /d' $myAliasFileTmpEnabled
    sed -i '/Checking/d' $myAliasFileTmpEnabled
    sed -i '/No release history was found for the requested project/d' $myAliasFileTmpEnabled
    sed -i '/Node no longer exists/d' $myAliasFileTmpEnabled
    sed -i '/The following module is missing/d' $myAliasFileTmpEnabled
    sed -i '/the documentation page/d' $myAliasFileTmpEnabled
    sed -i '/documentation page/d' $myAliasFileTmpEnabled
    sed -i '/page</a>/d' $myAliasFileTmpEnabled
    sed -i '/Project.php/d' $myAliasFileTmpEnabled

    myCommand="jq --raw-output 'to_entries[] | [.value.name, .value.existing_version, .value.latest_version, .value.recommended, .value.status_msg] | @csv' $myAliasFileTmpEnabled > $myAliasFileTmpEnabledCSV"
    eval $myCommand

    myAliasFileTmpDisabledCSV="./files/tmp/$myAlias-$mydate-disabled.csv"
    grep -Fvxf $myAliasFileTmpEnabledCSV $myAliasFileTmpCSV > $myAliasFileTmpDisabledCSV

    sed -e 's/$/,\"Disabled\"/' -i $myAliasFileTmpDisabledCSV
    sed -e 's/$/,\"Enabled\"/' -i $myAliasFileTmpEnabledCSV

    sed -i -e "s/^/\"$myAlias\",\"$mydate\",/" $myAliasFileTmpEnabledCSV
    sed -i -e "s/^/\"$myAlias\",\"$mydate\",/" $myAliasFileTmpDisabledCSV

    cat $myAliasFileTmpEnabledCSV >> $myFileName
    cat $myAliasFileTmpDisabledCSV >> $myFileName
  fi # commented line if check
done

### this part will write bash files that will update sites begin
cp $myFileName ./files/$mydate-livesites-updates-result-edited.csv
FILE="./files/$mydate-livesites-updates-result-edited.csv"

sed -i 's/\"//g' "$FILE"
sed -i 's/ /xxx/g' "$FILE"
tail -n +2 "$FILE" > "$FILE.tmp" && mv "$FILE.tmp" "$FILE"

mapfile -t updates < "$FILE"

mapfile -t allAliases < drupal_drush_ups_live_alias_names.txt

for alias in ${allAliases[*]};do
  IFS=',' read -ra myActualAliases <<< $alias

  if [[ ${alias:0:1} == "#" ]]  # commented line if check
  then
    # Skip it if commented
    echo "This one is skipped: ""$alias"
  else

    myActualAlias=${myActualAliases[0]}
    myType=${myActualAliases[1]}

    numberOfLines=$(grep -r "$myActualAlias" "$FILE" | wc -l)
    if [ $numberOfLines -gt 0 ]
    then
      myCommandFileFolder="./files/commands/$mydate-drush"
      mkdir -p $myCommandFileFolder
      myCommandFile="$myCommandFileFolder/$myActualAlias-$mydate-drush.sh"
      echo "# "$myActualAlias >> $myCommandFile
      echo "# Backup Drupal instance" >> $myCommandFile
      echo "drush @$myActualAlias ard" >> $myCommandFile

      for item in ${updates[*]}
        do
        IFS=',' read -ra mySiteInfo <<< $item

        myAlias=${mySiteInfo[0]}
        myModule=${mySiteInfo[2]}
        myActualVersion=${mySiteInfo[3]}
        myNewVersion=${mySiteInfo[5]}
        myStatus=${mySiteInfo[6]}
        myInstType=${mySiteInfo[7]}
        if [ $myActualAlias == $myAlias ]
        then
              if [ $myInstType == "Enabled" ]
              then
                echo "# Update $myModule from $myActualVersion --> $myNewVersion" >> $myCommandFile
                echo "# This module is $myInstType and the update message is '${myStatus//xxx/ }'" >> $myCommandFile
                echo "drush @$myAlias up $myModule -y" >> $myCommandFile
                echo "drush @$myActualAlias updb -y" >> $myCommandFile
              else
                echo "# Download $myModule and update code from $myActualVersion --> $myNewVersion" >> $myCommandFile
                echo "# This module is $myInstType and the update message is '${myStatus//xxx/ }'" >> $myCommandFile
                echo "drush @$myAlias dl $myModule -y" >> $myCommandFile
                echo "drush @$myActualAlias updb -y" >> $myCommandFile
              fi
        fi
      done
      if [ $myType == "D7" ]
        then
          echo "# Finally clear the caches" >> $myCommandFile
          echo "drush @$myActualAlias cc all" >> $myCommandFile
        else
          echo "# Finally clear the caches" >> $myCommandFile
          echo "drush @$myActualAlias cr" >> $myCommandFile
      fi
    fi
  fi # commented line if check
done
### this part will write bash files that will update sites end

# Delete files older than 7 days except .gitkeep files
find ./files* -type f -mtime +7 -not -name '.gitkeep' -delete

# Delete empty folder
find ./files -type d -empty -delete