<?php

/**
 * @file
 * This file contains drush alias examples.
 */

$aliases['myfirstsitealias'] = [
  // Root folder of Drupal installation.
  // drush will be executed here.
  // The folder where index.php of Drupal is found.
  'root' => '/root/folder/of/drupal/installation',
  // The site url. Note that this one is optional.
  'uri' => 'myfirstsite.com',
  // User on remote host that is permitted to access server via SSH.
  'remote-user' => 'remote_user',
  // Remote host. Either IP address or hostname.
  'remote-host' => 'remote_host',
];

$aliases['mysecondsitealias'] = [
  // Root folder of Drupal installation.
  // drush will be executed here.
  // The folder where index.php of Drupal is found.
  'root' => '/root/folder/of/drupal/installation',
  // The site url. Note that this one is optional.
  'uri' => 'mysecondsite.com',
  // User on remote host that is permitted to access server via SSH.
  'remote-user' => 'remote_user',
  // Remote host. Either IP address or hostname.
  'remote-host' => 'remote_host',
];
