#!/bin/bash
# must be run like this
# bash script.sh

cp livesite.aliases.drushrc.php $HOME/.drush/livesite.aliases.drushrc.php
drush cc drush

mydate=$(date +%Y%m%d_%H%M%S)

# Creating aliases array with values coming from the text file.
mapfile -t aliases < drupal_drush_ups_live_alias_names.txt


touch ./files/$mydate-livesites-updates.csv
echo "SiteName,Date,ModulName,OldVersion,NewVersion,Status" >> ./files/$mydate-livesites-updates.csv

for item in ${aliases[*]}
do
  IFS=',' read -r -a mySiteInfo <<< "$item"
  myAlias=${mySiteInfo[0]}
  myType=${mySiteInfo[1]}
  echo "$myAlias is being checked..."
  if [ $myType == "D7" ]
  then
    drush @$myAlias cc all
  else
    drush @$myAlias cr
  fi
  drush @$myAlias ups --format=csv --check-disabled > ./files/tmp/$myAlias-$mydate.csv
  sed -i '/Kullanılabilir güncelleme/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/güncelleme verisi kontrol ediliyor /d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/Checking/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/No release history was found for the requested project/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/Node no longer exists/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/The following module is missing/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/the documentation page/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i '/page</a>/d' ./files/tmp/$myAlias-$mydate.csv
  sed -i -e "s/^/$myAlias,$mydate,/" ./files/tmp/$myAlias-$mydate.csv
  cat  ./files/tmp/$myAlias-$mydate.csv >> ./files/$mydate-livesites-updates.csv
done

# Delete files older than 7 days except .gitkeep files
find ./files* -type f -mtime +7 -not -name '.gitkeep' -delete