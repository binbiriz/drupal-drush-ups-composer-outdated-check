#!/bin/bash
# must be run like this
# bash drupal_composer_outdated_json_live.sh

mydate=$(date +%Y%m%d_%H%M%S)

# Creating aliases array with values coming from the text file.
mapfile -t aliases < drupal_composer_outdated_json_live_path_names.txt

touch ./files/$mydate-livesites-updates-composer.csv
echo "SiteName,Date,ModulName,OldVersion,NewVersion,Status" >> ./files/$mydate-livesites-updates-composer.csv

touch ./files/$mydate-livesites-updates-composer.csv
echo "SiteName,Date,ModulName,CurrentVersion" >> ./files/$mydate-livesites-updates-composer-sec.csv

for item in ${aliases[*]}
do
  IFS=',' read -r -a mySiteInfo <<< "$item"

  if [[ ${item:0:1} == "#" ]]
  then
    # Skip it if commented
    echo "This one is skipped: ""$item"
  else
    # Do it it if not commented
    echo "Doing: ""$item"
    mySite=${mySiteInfo[0]}
    myUser=${mySiteInfo[1]}
    myHost=${mySiteInfo[2]}
    myPath=${mySiteInfo[3]}
    echo "$mySite is being checked..."
    myCommand="ssh -t $myUser@$myHost 'composer --working-dir=$myPath outdated drupal/* --format=json' > ./files/tmp/$mySite-$mydate.json"
    #echo $myCommand
    eval $myCommand
    myCommand="jq --raw-output '.[] | .[] | [.name, .version, .latest, .description]| @csv' ./files/tmp/$mySite-$mydate.json > ./files/tmp/$mySite-$mydate.csv"
    eval $myCommand
    myCommand="drush @$mySite pm:security --format=json > ./files/tmp/$mySite-$mydate-sec.json"
    eval $myCommand
    sed -i '/\[warning\]/d' ./files/tmp/$mySite-$mydate-sec.json
    sed -i '/\[notice\]/d' ./files/tmp/$mySite-$mydate-sec.json
    sed -i '/\[success\]/d' ./files/tmp/$mySite-$mydate-sec.json
    myCommand="jq --raw-output '.[] | [.name, .version]| @csv' ./files/tmp/$mySite-$mydate-sec.json > ./files/tmp/$mySite-$mydate-sec.csv"
    eval $myCommand

    while read line
    do
      echo "\"$mySite\",\"$mydate\",$line" >> ./files/$mydate-livesites-updates-composer.csv
    done <./files/tmp/$mySite-$mydate.csv

    while read line
    do
      echo "\"$mySite\",\"$mydate\",$line" >> ./files/$mydate-livesites-updates-composer-sec.csv
    done <./files/tmp/$mySite-$mydate-sec.csv

    myCommandFileFolder="./files/commands/$mydate-composer"
    mkdir -p $myCommandFileFolder
    myCommandFile="$myCommandFileFolder/$mySite-$mydate-composer.sh"

    while read line
    do
      IFS=',' read -r -a myCommandArray <<< "$line"

      myModule=${myCommandArray[0]}
      # Remove double quotes from string variable
      myModule=$(sed -e 's/^"//' -e 's/"$//' <<<"$myModule")
      myModuleCurrentVersion=${myCommandArray[1]}
      # Remove double quotes from string variable
      myModuleCurrentVersion=$(sed -e 's/^"//' -e 's/"$//' <<<"$myModuleCurrentVersion")
      myModuleNewVersion=${myCommandArray[2]}
      # Remove double quotes from string variable
      myModuleNewVersion=$(sed -e 's/^"//' -e 's/"$//' <<<"$myModuleNewVersion")

      echo "# Update $myModule from $myModuleCurrentVersion to $myModuleNewVersion" >> $myCommandFile

      if [ $myModule == "drupal/core" ]
      then
        myText="lando composer update $myModule webflo/drupal-core-require-dev --with-dependencies --no-interaction"
      else
        myText="lando composer update $myModule --with-dependencies --no-interaction"
      fi

      echo $myText >> $myCommandFile
      # echo "drush updb -y" >> $myCommandFile
      # echo "drush cr -y" >> $myCommandFile
      echo "lando drush updb -y && lando drush cr -y" >> $myCommandFile
      echo "git status" >> $myCommandFile
      echo "git add ." >> $myCommandFile
      echo "git commit -m \"Update $myModule ($myModuleCurrentVersion => $myModuleNewVersion)\"" >> $myCommandFile
      echo "lando drush cex -y" >> $myCommandFile
      echo "git status" >> $myCommandFile
      echo "git add ." >> $myCommandFile
      echo "git commit -m \"Update $myModule, config export if any\"" >> $myCommandFile
      echo "lando drush cr -y" >> $myCommandFile
    done <./files/tmp/$mySite-$mydate.csv

    # rm ./files/tmp/$mySite-$mydate.json
  fi # commented line if check
done

# Delete files older than 7 days except .gitkeep files
find ./files* -type f -mtime +7 -not -name '.gitkeep' -delete